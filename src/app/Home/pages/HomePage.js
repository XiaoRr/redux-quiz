import React, {Component} from 'react';
import {connect} from "react-redux";
import {getAllArticles} from '../actions/homeActions';
import {bindActionCreators} from "redux";
import {Link} from "react-router-dom";

class HomePage extends Component {
  componentDidMount() {
    this.props.getAllArticles();
  }


  renderTitleList(articles) {
    console.log("articles",articles);
    if(articles != null)
      return (
        articles.map((article) => {
          return (<Link key = {article.id}
                exact = "true"
                to = {"/api/posts/"+article.id}>
            {article.title}<br/>
          </Link>)
        })
      )
  }

  render() {
    const {articles} = this.props;
    return (
      <div className="">
        {this.renderTitleList(articles)}>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  articles: state.homeReducer.articles
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllArticles
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
