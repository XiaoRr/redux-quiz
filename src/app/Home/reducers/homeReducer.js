
const initState = {
  articles: []
};

const homeReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_ARTICLES":
      return {
        ...state,
        articles:action.articles
      };
    default:
      return state
  }
};

export default homeReducer;
