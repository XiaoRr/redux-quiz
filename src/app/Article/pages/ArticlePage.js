import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Link} from "react-router-dom";
import {getAllArticles} from "../../Home/actions/homeActions";
import warning from "react-redux/lib/utils/warning";
import {deleteArticle} from "../../../resources/getDataFromApi";

class ArticlePage extends Component {

  constructor(props) {
    super(props);
    //this.deleteArticle = this.deleteArticle.bind(this);
  }
  componentDidMount() {
    this.props.getAllArticles();
  }


  renderTitleList(articles) {
    console.log("articles",articles);
    if(articles != null)
      return (
        articles.map((article) => {
          return (<Link key = {article.id}
                exact = "true"
                to = {"/api/posts/"+article.id}>
            {article.title}&nbsp;&nbsp;
          </Link>)
        })
      )
  }

  renderArticle(articles){
    const {id} = this.props.match.params;
    let index = -1;
    for(let i in articles){
      if(articles[i].id + '' === id){
        index = i;
      }
    }
    if(index !== -1)
      return (
        <div>
          <div>{articles[index].title}</div>
          <div>{articles[index].description}</div>
          <button onClick={this.deleteArticle.bind(this,id)}>delete</button>
        </div>

      )
  }

  deleteArticle(id){
    deleteArticle(id).then(
      location.reload()
    )
  }

  render() {
    const {articles} = this.props;
    return (
      <div className="">
        {this.renderTitleList(articles)}
        {this.renderArticle(articles)}
      </div>
    );
  }

  renderDeleteBtn() {
    const {id} = this.props.match.params;
  }
}

const mapStateToProps = state => ({
  articles: state.homeReducer.articles
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllArticles
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ArticlePage);
