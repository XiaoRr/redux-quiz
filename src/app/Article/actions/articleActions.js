import Axios from "axios";

export const getAllArticles = () =>{
  return (dispatch) => {
    Axios.get("http://localhost:8080/api/posts")
      .then((res) =>{
        dispatch({type: "GET_ALL_ARTICLES", articles: res.data});
        console.log("async get",res.data)
      })
  }
};