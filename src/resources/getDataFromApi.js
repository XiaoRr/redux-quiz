import Axios from "axios";

const url = "http://localhost:8080/";

function getAllArticle(){
  return Axios.get(`${url}api/posts`)
    .then(function (response) {
      console.log(response);
      return response.data;
    })
}

/*
  return (dispatch) => {
    dispatch({type: CREATE_ORGANIZATION});
    axios.post('/url', values)
      .then((res) =>{
        dispatch({type: "GET_ALL_ARTICLES", payload: res});
      })
      .catch((error)=> {
        dispatch({type: "GET_ALL_ARTICLES", payload: error});
      })
  }
 */

function createArticle(title,description){
  let data = {"title":title,"description":description}
  Axios.post(
    `${url}api/posts`,data)
    .then(function (response) {
    console.log(response);
    return response.data;
  })
}

function deleteArticle(id){
  return Axios.delete(`${url}api/posts/${id}`)
    .then(function (response) {
      console.log(response);
      return response.ok;
    })
}
export {getAllArticle, createArticle, deleteArticle};