import React, {Component} from 'react';
import './App.less';
import {createArticle, deleteArticle, getAllArticle} from "./resources/getDataFromApi";
import HomePage from "./app/Home/pages/HomePage";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import ArticlePage from "./app/Article/pages/ArticlePage";

class App extends Component{

  constructor(props){
    super(props);
  }

  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route path="/" exact component={HomePage}/>
            <Route path="/api/posts/:id" component={ArticlePage}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;